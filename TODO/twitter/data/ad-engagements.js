window.YTD.ad_engagements.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 04:18:36"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 04:18:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 03:58:27"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 04:18:38",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:58:38",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 03:58:32",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 03:58:34",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:58:31",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:58:36",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 03:58:38",
            "engagementType" : "VideoContentPlayback95"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514947989671937",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 03:17:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 03:17:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 03:17:12",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:17:12",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-09 03:17:07",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 03:17:09",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 03:17:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:17:07",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 03:17:05",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:17:05",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-09 03:17:09",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 03:17:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 03:26:29",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 03:26:32",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-09 03:26:34",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-09 03:26:28",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 03:26:27",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 03:26:35",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 03:26:25",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:35:49",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:26:29",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-09 03:26:32",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 03:26:30",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:26:27",
            "engagementType" : "VideoContentPlayback25"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 07:45:40"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 07:45:46",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 07:45:53",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 07:45:42",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-09 07:45:49",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 07:45:54",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 07:45:43",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514844566597635",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 11:19:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 11:20:47",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 11:19:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 11:20:00",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 11:20:05",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 11:20:03",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 11:20:01",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 11:20:08",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 11:20:07",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-09 11:20:01",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 11:20:02",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-09 11:20:02",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 11:20:31",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 11:19:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 11:20:05",
            "engagementType" : "VideoContent6secView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1093107212556345344",
              "tweetText" : "Private virtual Platform as a Service for developers.\nLots of features available from the box.\nTry for free!\n https://t.co/6cuN6zzRhI \n#d2cio #docker #4devs https://t.co/37UxQaNQxg",
              "urls" : [ "https://t.co/6cuN6zzRhI" ],
              "mediaUrls" : [ "https://t.co/37UxQaNQxg" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "D2C.io",
              "screenName" : "@d2cio"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@github"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Azure"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ThePSF"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nodejs"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@MongoDB"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@GCPcloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ubuntu"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Linux"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@debian"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@codinghorror"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Docker"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "devops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "linux"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "golang"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "javascript"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "github"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "python"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#golang"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-10 06:39:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:40:25",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:40:31",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240578761495425040",
              "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 06:22:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:22:10",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-10 06:22:11",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:22:14",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-10 06:22:14",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-10 06:22:09",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-10 06:22:17",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-10 06:23:16",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:22:10",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:22:11",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-10 06:22:12",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-10 06:22:08",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:22:17",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1181307044550381569",
              "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
              "urls" : [ "https://t.co/MnoLgq4YuK" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@rapid7"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#DevOps"
            }, {
              "targetingType" : "Tailored audiences (lists)",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-10 06:39:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:40:25",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:40:17",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:40:18",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:40:15",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:40:16",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-10 06:40:18",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1247479734000406528",
              "tweetText" : "Welche Chancen haben Menschen ohne Zugang zu funktionierenden medizinischen und sanitären Einrichtungen im Kampf gegen #COVIDー19 ?\n\nGemeinsam müssen wir jenen helfen, die gefährdet sind.\n👉 https://t.co/SMynVT5eLg https://t.co/2L5gWypIVg",
              "urls" : [ "https://t.co/SMynVT5eLg" ],
              "mediaUrls" : [ "https://t.co/2L5gWypIVg" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "IKRK",
              "screenName" : "@IKRK"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ICRC"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@RotesKreuz_CH"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 06:22:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:22:49",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-10 06:22:43",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-10 06:22:37",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:22:55",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-10 06:22:43",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-10 06:25:18",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:23:00",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-10 06:22:41",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:22:41",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-10 06:23:01",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-10 06:22:39",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:22:39",
            "engagementType" : "VideoContent1secView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 12:08:58"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 14:38:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 14:39:00",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Dropbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-24 07:29:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-24 07:30:00",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-24 07:29:58",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-24 07:29:56",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-24 07:32:23",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-24 07:29:58",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-24 07:29:55",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContent6secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-24 07:29:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-24 07:29:51",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-24 07:29:34",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-24 07:29:56",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1275135887354089474",
              "tweetText" : "Chokes are EMC components that share aspects with inductors and transformers. Join our June 24 #webinar at 9 A.M. EDT/3 P.M. CEST to explore the construction and characteristics of different types of chokes: https://t.co/KLltIkHKrJ #KEMETishere for #engineers #engineering #tech https://t.co/ghSYh8hZ5w",
              "urls" : [ "https://t.co/KLltIkHKrJ" ],
              "mediaUrls" : [ "https://t.co/ghSYh8hZ5w" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "KEMET Electronics",
              "screenName" : "@KEMETCapacitors"
            },
            "impressionTime" : "2020-06-24 01:01:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-06-24 01:01:26",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-06-24 01:01:03",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-06-24 01:01:09",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-06-24 01:01:24",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-06-24 01:02:12",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-06-24 01:01:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-06-24 01:01:14",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-06-24 01:01:20",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-06-24 01:01:05",
            "engagementType" : "VideoContentMrcView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1270656956504641537",
              "tweetText" : "#Barrierefrei posten: In den Barrierefreiheit-Einstellungen \"Bildbeschreibungen verfassen\" aktivieren. Beim Posten eines Fotos unten rechts auf den +ALT-Button tippen und Text eingeben.\n#gemeinsambereit #bereit https://t.co/90SAooVT4y",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/90SAooVT4y" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@amnesty"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-06-26 21:37:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-06-26 21:44:00",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-06-26 21:38:16",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-06-26 21:37:56",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-06-26 21:38:03",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-06-26 21:37:53",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-06-26 21:38:00",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-06-26 21:38:09",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-06-26 21:38:12",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-06-26 21:38:05",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-06-26 21:37:54",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
} ]