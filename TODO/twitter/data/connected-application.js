window.YTD.connected_application.part0 = [ {
  "connectedApplication" : {
    "organization" : {
      "name" : "Twitter, Inc.",
      "url" : ""
    },
    "name" : "Twitter for Android",
    "description" : "Twitter for Android",
    "permissions" : [ "read", "write" ],
    "approvedAt" : "2020-07-07T01:56:29.000Z",
    "id" : "258901"
  }
}, {
  "connectedApplication" : {
    "organization" : {
      "name" : "",
      "privacyPolicyUrl" : "https://www.unfollowspy.com/privacy.html",
      "termsAndConditionsUrl" : "https://www.unfollowspy.com/terms.html"
    },
    "name" : "Unfollowspy",
    "description" : "Audience analysis, User management, post scheduled tweets and more.",
    "permissions" : [ "read", "write", "directmessages" ],
    "approvedAt" : "2020-01-05T03:09:35.000Z",
    "id" : "4198440"
  }
} ]